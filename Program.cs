﻿using System;
using FileManagement.UserInteraction;

namespace FileManagement
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            Console.Title = "Filemanager";
            Menus.MainMenu();
        }
    }
}
