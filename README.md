# Filemanagement system

## Menus
* Creates Menu instances
* Contains all menus/"Option trees" shown to the user and the logic behind them.
* Invokes methods in Services

### How each menu works
* Each menu prints choices to screen
* Then takes a key from user and passes it through an switch-statement (or if in one case)
* The switch contains logic for what comes next

## InputReader
* Reads input from user (int/string) and returns value

## FileService
* Contains all logic that manipulates files
* Each method also send a Logging-Request

## LoggingService
* Starts and stops the logging watch and logs to file

## PrintingService
* Contains all logic for when choices are requested printed to screen

##Enums
* Contains constants, like file locations (Bad Idea for these filepaths, they are relative paths)