﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
namespace FileManagement.Services
{
    class FileService
    {


        public static FileInfo[] GetFilesInDirectory(string directory)
        {
            LoggingService.StartLog();

            DirectoryInfo di = new DirectoryInfo(directory);
            FileInfo[] Files = di.GetFiles();

            LoggingService.StopLog($"{Files.Length} files was fetched from the resources directory");
            return Files;
        }

        public static FileInfo GetFileInDirectory(string directory, string fileName)
        {
            LoggingService.StartLog();

            DirectoryInfo di = new DirectoryInfo(directory);
            FileInfo[] Files = di.GetFiles(fileName);

            LoggingService.StopLog($"File {fileName} was requested from directory");

            return Files[0];
        }

        public static List<string> GetLinesInFile(FileInfo file)
        {
            LoggingService.StartLog();

            List<string> lines = new List<string>();
            try
            {
                FileStream fs = file.OpenRead();
                StreamReader sr = new StreamReader(fs);
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    lines.Add(line);
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }

            LoggingService.StopLog($"Lines from file {file.Name} was returned");

            return lines;
        }

        internal static int GetWordOccurrencesInFile(string searchQuery, FileInfo file)
        {
            LoggingService.StartLog();

            int occurrences = 0;

            try
            {
                FileStream fs = file.OpenRead();
                StreamReader sr = new StreamReader(fs);
                string line;
                searchQuery = searchQuery.ToLower();
                while ((line = sr.ReadLine()) != null)
                {
                    line = line.ToLower();
                    //Removes punctuation and splits line on " ".
                    string[] strings = line.Where(c => !char.IsPunctuation(c))
                                            .Aggregate("", (current, c) => current + c).Split(' ');
                    foreach (string s in strings)
                    {
                        //Will only match whole words
                        if (s.Equals(searchQuery))
                        {
                            occurrences++;
                        }
                    }
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }

            LoggingService.StopLog($"The term {searchQuery} was found {occurrences} times in file {file.Name}");

            return occurrences;
        }

        internal static List<FileInfo> GetFilesWithExtensionX(FileInfo[] Files, string extensionX)
        {
            LoggingService.StartLog();

            List<FileInfo> files = Files.ToList().FindAll(x => x.Extension == extensionX);

            LoggingService.StopLog($"Files with extension {extensionX} was printed in the console");

            return files;
        }
    }
}
