﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace FileManagement.Services
{
    public class LoggingService
    {
        static Stopwatch Stopwatch = new Stopwatch();
        public static void WriteToLog(string message, int duration) {
            DateTime dateTime = DateTime.Now;

            try
            {
                using (StreamWriter streamWriter = new StreamWriter(Enums.LOG, true))
                {
                    streamWriter.WriteLine($"{dateTime}: {message}. The function took {duration}ms to execute.");
                }
            }
            catch (Exception e )
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void StartLog()
        {
            Stopwatch.Restart();
            Stopwatch.Start();
        }

        public static void StopLog( string message )
        {
            Stopwatch.Stop();
            WriteToLog(message, (int)Stopwatch.ElapsedMilliseconds);
        }
    }
}
