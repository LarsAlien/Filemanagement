﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FileManagement.Services
{
    class PrintingService
    {
        public static void PrintFilesInDirectory(string directory)
        {
            LoggingService.StartLog();

            FileInfo[] Files = FileService.GetFilesInDirectory(directory);
            foreach (FileInfo file in Files)
            {
                Console.WriteLine(file.Name);
            }
            LoggingService.StopLog($"Listed {Files.Length} files from the resources directory");
        }

        internal static void PrintFileName(FileInfo file)
        {
            LoggingService.StartLog();

            Console.WriteLine("FileName: " + file.Name);

            LoggingService.StopLog($"File name {file.Name} was written to the console");
        }

        internal static void PrintFileLengthInBytes(FileInfo file)
        {
            LoggingService.StartLog();

            Console.WriteLine("Size of file: " + file.Length + " bytes");

            LoggingService.StopLog($"Size of {file.Name} ({file.Length}bytes) was written to the console");
        }

        internal static void PrintNumberOfLinesInFile(FileInfo file)
        {
            LoggingService.StartLog();

            int length = FileService.GetLinesInFile(file).Count;
            Console.WriteLine($"Number of lines in file: {length}");

            LoggingService.StopLog($"Number of lines in file {file.Name} ({length}) was written to the console");
        }

        public static void PrintFilesWithExtensionX(FileInfo[] Files, string extensionX)
        {
            LoggingService.StartLog();

            List<FileInfo> files = FileService.GetFilesWithExtensionX(Files, extensionX);

            foreach (FileInfo file in files)
            {
                Console.WriteLine(file.Name);
            }
            LoggingService.StopLog($"Files with extension {extensionX} was written to the console");

        }

        public static void PrintWordOccurencesInFile( string searchQuery, FileInfo file)
        {
            LoggingService.StartLog();

            int occurrences = FileService.GetWordOccurrencesInFile(searchQuery, file);
            Console.WriteLine($"The term {searchQuery} was found {occurrences} times in file {file.Name}");
            LoggingService.StopLog($"Number of occurrences of the word {searchQuery} in file {file.Name} was written to the console");

        }
    }
}
