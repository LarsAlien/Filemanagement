﻿using System;

namespace FileManagement.UserInteraction
{
    static class InputReader
    {
        public static int ConsoleReadInt()
        {
            Console.WriteLine();
            Console.WriteLine("Enter number: ");
            string readInt = Console.ReadLine();
            return int.TryParse(readInt, out int j)
                    ? j
                    : ConsoleReadInt();
        }

        public static string ConsoleReadString()
        {
            Console.WriteLine();
            Console.WriteLine("Enter string: ");
            string readString = Console.ReadLine();
            return readString is string
                    ? readString
                    : ConsoleReadString();
        }
    }
}
