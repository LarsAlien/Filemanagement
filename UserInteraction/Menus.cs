﻿using FileManagement.Services;
using System;
using System.Collections.Generic;
using System.IO;

namespace FileManagement.UserInteraction
{
    class Menus
    {
        public static void MainMenu()
        {
            IDictionary<int, string> options = new Dictionary<int, string>()
            {
                {1, "List all files in resources directory"},
                {2, "Get files by extension"},
                {3, "Manipulate Dracula"},
                {4, "Exit application"},
            };
            Menu mainMenu = new Menu(options);

            Console.WriteLine();

            int inputKey = mainMenu.StartMenu();

            switch (inputKey)
            {
                case 1:
                    PrintingService.PrintFilesInDirectory(Enums.RESOURCES);
                    MainMenu();
                    break;
                case 2:
                    FileInfo[] Files = FileService.GetFilesInDirectory(Enums.RESOURCES);
                    FileExtensionMenu(Files);
                    break;
                case 3:
                    DraculaMenu();
                    break;
                case 4:
                    Environment.Exit(0);
                    break;

                default:
                    MainMenu();
                    break;
            }
        }

        public static void DraculaMenu()
        {
            IDictionary<int, string> options = new Dictionary<int, string>()
            {
                {1, "Get filename"},
                {2, "Get size of file"},
                {3, "Get number of lines in file"},
                {4, "Search for word in file"},
                {5, "Return to main menu"},
            };

            Menu draculaMenu = new Menu(options);

            Console.WriteLine();
            int inputKey = draculaMenu.StartMenu();

            FileInfo draculaFile = FileService.GetFileInDirectory(Enums.RESOURCES, "Dracula.txt");

            switch (inputKey)
            {
                case 1:
                    PrintingService.PrintFileName(draculaFile);
                    DraculaMenu();
                    break;
                case 2:
                    PrintingService.PrintFileLengthInBytes(draculaFile);
                    DraculaMenu();
                    break;
                case 3:
                    PrintingService.PrintNumberOfLinesInFile(draculaFile);
                    DraculaMenu();
                    break;

                case 4:
                    SearchOption();
                    break;

                case 5:
                    MainMenu();
                    break;

                default:
                    DraculaMenu();
                    break;
            }

            void SearchOption()
            {
                Console.WriteLine();

                Console.WriteLine("What do you want to search for?(Type 'q' to exit)");
                string key = InputReader.ConsoleReadString();

                switch (key)
                {
                    case "q":
                        DraculaMenu();
                        break;

                    default:
                        PrintingService.PrintWordOccurencesInFile(key, draculaFile);
                        SearchOption();
                        break;
                }
            }
        }

        public static void FileExtensionMenu(FileInfo[] Files)
        {
            IDictionary<int, string> options = new Dictionary<int, string>();
            HashSet<string> hashSet = new HashSet<string>();
            for (int i = 0; i < Files.Length; i++)
            {
                // If extension on index i is added to hashset, then it is unique and added to options
                if (hashSet.Add(Files[i].Extension)) options.Add(options.Count + 1, Files[i].Extension);
            }
            options.Add(options.Count + 1, "Return to main menu");

            Menu draculaMenu = new Menu(options);

            Console.WriteLine();

            int inputKey = draculaMenu.StartMenu();

            if (inputKey == options.Count)
            {
                MainMenu();
            }
            else
            {
                PrintingService.PrintFilesWithExtensionX(Files, options[inputKey]);
                FileExtensionMenu(Files);
            }
        }
    }
}
