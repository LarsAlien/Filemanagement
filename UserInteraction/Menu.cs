﻿using System;
using System.Collections.Generic;

namespace FileManagement.UserInteraction
{
    public class Menu
    {
        static IDictionary<int, string> Options { get; set; }

        public Menu(IDictionary<int, string> options)
        {
            Options = options;
        }

        public int StartMenu()
        {
            foreach (KeyValuePair<int, string> entry in Options)
            {
                Console.WriteLine(entry.Key + " : " + entry.Value);
            }
            int inputKey = InputReader.ConsoleReadInt();

            return inputKey;
        }
    }
}
